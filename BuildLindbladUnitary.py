'''
Created on Sep 19, 2016

@author: cs9114
'''

import numpy

def BuildLindUnitary(U):
    """
    This function rotates one lindblad to from one basis to another. In the simple wavefunction space the transformation is done by U.
    """
    
    assert len(U.shape)==2
    assert U.shape[0]==U.shape[1]
    numpy.testing.assert_allclose(numpy.dot(numpy.transpose(U),U),numpy.eye(len(U)),atol=1e-4)
    
    Nsys=len(U)
    
    RotMatrix=numpy.zeros(shape=(Nsys**2,Nsys**2))
    for a1 in range (0,Nsys,1):
        for a2 in range (0,Nsys,1):
                for z1 in range (0,Nsys,1):
                    for z2 in range (0,Nsys,1):
#                         RotMatrix[(z1*Nsys)+z2,(a1*Nsys)+a2]=U[a1,z1]*U[a2,z2]
                        RotMatrix[(z1*Nsys)+z2,(a1*Nsys)+a2]=U[a1,z1]*U[a2,z2]
                        
    numpy.testing.assert_allclose(numpy.dot(numpy.transpose(RotMatrix),RotMatrix),numpy.eye(len(RotMatrix)),atol=1e-4)
    
    
    return RotMatrix


# def RotateLindMatrix(Lind,U):
#     """
#     This function rotates one lindblad to from one basis to another. In the simple wavefunction space the transformation is done by U.
#     """
#     
#     assert Lind.shape[0]==Lind.shape[1]
#     assert Lind.shape[0]==U.shape[0]**2
#     assert U.shape[0]==U.shape[1]
#     assert len(U.shape)==len(Lind.shape)
#     assert len(U.shape)==2
#     
#     Nsys=len(U)
#     LindU=numpy.zeros(shape=Lind.shape)
#     
# ##########     Long Method     #######
# #     for a1 in range (0,Nsys,1):
# #         for a2 in range (0,Nsys,1):
# #             for b1 in range (0,Nsys,1):
# #                 for b2 in range (0,Nsys,1):
# #                     for z1 in range (0,Nsys,1):
# #                         for z2 in range (0,Nsys,1):
# #                             for y1 in range (0,Nsys,1):
# #                                 for y2 in range (0,Nsys,1):
# #                                     LindU[(a1*Nsys)+a2,(b1*Nsys)+b2]+=U[a1,z1]*U[b1,y1]*Lind[(z1*Nsys)+z2,(y1*Nsys)+y2]*U[b2,y2]*U[a2,z2]
# 
#     RotMatrix=numpy.zeros(shape=Lind.shape)
#     for a1 in range (0,Nsys,1):
#         for a2 in range (0,Nsys,1):
#                 for z1 in range (0,Nsys,1):
#                     for z2 in range (0,Nsys,1):
#                         RotMatrix[(z1*Nsys)+z2,(a1*Nsys)+a2]=U[a1,z1]*U[a2,z2]
#                         
#     LindU=numpy.dot(numpy.transpose(RotMatrix),numpy.dot(Lind,RotMatrix))
#     
#     
#     
#     
# 
#     numpy.testing.assert_allclose(numpy.sort(numpy.linalg.eigvals(Lind)),numpy.sort(numpy.linalg.eigvals(LindU)),rtol=0.01)
# 
# #     print ''
# #     numpy.set_printoptions(formatter={'float': '{: 0.3f}'.format})
# #     print "blah"
# #     print blah
# #     print ""
# #     print numpy.round(LindU,2)
# #     print ""
# #     print numpy.linalg.eigvals(Lind)
# #     print numpy.linalg.eigvals(LindU)
# #     numpy.set_printoptions()
# 
#     return LindU

if __name__ == '__main__':
    pass
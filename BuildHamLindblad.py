'''
Created on Sep 19, 2016

@author: cs9114
'''

import numpy

def BuildHamLindblad(Input):
    """
    Build a lindblad Hamiltonian in a eigenbasis. state ordering is ground-> then excited
    """
    
    assert len(Input.shape)==1 or len(Input.shape)==2
    if len(Input.shape)==1:
        EigenEnegy=Input
        
        Nsite=len(EigenEnegy)
        Nsys=Nsite+1
        Lind=numpy.zeros(shape=(Nsys**2,Nsys**2),dtype=complex)
        
        if len(EigenEnegy.shape)==1:
            for i in range(1,Nsys,1):
                Lind[(i*Nsys)+0,(i*Nsys)+0]=-1j*EigenEnegy[i-1]
                Lind[(0*Nsys)+i,(0*Nsys)+i]=numpy.conjugate(Lind[(i*Nsys)+0,(i*Nsys)+0])
                for j in range(i+1,Nsys,1):
                    Lind[(i*Nsys)+j,(i*Nsys)+j]=-1j*(EigenEnegy[i-1]-EigenEnegy[j-1])
                    Lind[(j*Nsys)+i,(j*Nsys)+i]=numpy.conjugate(Lind[(i*Nsys)+j,(i*Nsys)+j])
                
    
    elif len(Input.shape)==2:
        assert Input.shape[0]==Input.shape[1]
        Ham=Input
        
        Nsys=len(Ham)
        Lind=numpy.zeros(shape=(Nsys**2,Nsys**2),dtype=complex)
        
        for a in range(0,Nsys,1):
            for b in range(0,Nsys,1):
                for i in range(0,Nsys,1):
                    Lind[(a*Nsys)+b,(a*Nsys)+i]+=1j*Ham[i,b]
                    Lind[(a*Nsys)+b,(i*Nsys)+b]-=1j*Ham[a,i]
        
        return Lind
        


    return Lind


def Step_HamLindblad(IS,EigenEnegy,tS):
    """
    
    """
    
    assert len(EigenEnegy.shape)==1
    assert numpy.all(EigenEnegy*tS<0.1)
    
    Nsite=len(EigenEnegy)
    Nsys=Nsite+1
    
    assert IS.shape==(Nsys,Nsys)
    rhoDot=numpy.zeros(shape=(IS.shape),dtype=complex)
    
    for i in range(1,Nsys,1):
        rhoDot[i,0]=-1j*(EigenEnegy[i-1])*IS[0,i]*IS[i,0]
        rhoDot[0,i]=numpy.conjugate(rhoDot[i,0])
        for j in range(i+1,Nsys,1):
            rhoDot[i,j]=-1j*(EigenEnegy[i-1]+EigenEnegy[j-1])*IS[i,j]
            rhoDot[j,i]=numpy.conjugate(rhoDot[i,j])


    return rhoDot*tS

if __name__ == '__main__':
    pass
import numpy

def BuildPhotonLindblad(EinA,Field,pEinA,pField):
    """
    This function produces a lindblad matrix of size (n+1)^2 x (n+1)^2 where n is the number of excited eigenstate of the system. (The +1 is from the groundstate).
    See \cite{Tscherbul2015a}, AnisotropyPaper_CS_resubmit.tex and LightMatter_Lindblad for details.
    
    EinA is a 1D matrix of einstein a coefficient
    Field is a 1D matrix of pumping rates if there are no unactive sites
    Field is a 2D matrix of pumping and relaxing rates if there are dark sites
    pEinA is a 2D matrix of the dot products of the transition dipole moment unit vectors
    pField is a 2D matrix of the dot products of the cross products of each site pair with the pumping radiation
    """
    
    assert len(EinA.shape)==1
    assert len(Field.shape)==1 or len(Field.shape)==2
    assert len(pEinA.shape)==2
    assert pEinA.shape==pField.shape
    
    Nsite=len(EinA)
    if len(Field.shape)==1:
        assert len(Field)==Nsite
        FieldEx=Field
        FieldRe=Field
    if len(Field.shape)==2:
        assert len(Field)==2 and len(Field[0])==Nsite
        FieldEx=Field[0]
        FieldRe=Field[1]
    
    LindEigen=numpy.zeros(shape=((Nsite+1)**2,(Nsite+1)**2))
    
    
        
    for i in range(0,Nsite):
        LindEigen[0,0]+=-pField[i,i]*FieldEx[i] #Bleaching out of \rho_{gg} to \rho_{i,j}
        
        
        for j in range(0,Nsite):
            
            LindEigen[0,((i+1)*(Nsite+1))+j+1]=((pEinA[i,j]*(EinA[i]*EinA[j])**0.5)+(pField[i,j]*(FieldRe[i]*FieldRe[j])**0.5)) #de-excitation and relaxation \rho_{i,j} to \rho_{0,0}
            
            
            
            LindEigen[((i+1)*(Nsite+1)),((j+1)*(Nsite+1))]+=-0.5*((pEinA[i,j]*(EinA[i]*EinA[j])**0.5)+(pField[i,j]*(FieldRe[i]*FieldRe[j])**0.5)) # \rho_{g,j} to \rho_{g,i}
            
            LindEigen[((i+1)*(Nsite+1)),((i+1)*(Nsite+1))]+=-0.5*FieldEx[j] #Bleaching out of \rho_{g,i} to \rho_{j,i}
            LindEigen[i+1,j+1]+=-0.5*((pEinA[i,j]*(EinA[i]*EinA[j])**0.5)+(pField[i,j]*(FieldRe[i]*FieldRe[j])**0.5)) # \rho_{j,g} to \rho_{i,g}
            
            LindEigen[i+1,i+1]+=-0.5*FieldEx[j] #Bleaching out of \rho_{i,g} to \rho_{i,j}
            LindEigen[((i+1)*(Nsite+1))+j+1,0]=pField[i,j]*(FieldEx[i]*FieldEx[j])**0.5 #Excitation into \rho_{i,j} from \rho_{gg}
            
            for k in range(0,Nsite):
                LindEigen[((i+1)*(Nsite+1))+j+1,((k+1)*(Nsite+1))+j+1]+=-0.5*((pEinA[i,k]*(EinA[i]*EinA[k])**0.5)+(pField[i,k]*(FieldRe[i]*FieldRe[k])**0.5))
                LindEigen[((i+1)*(Nsite+1))+j+1,((i+1)*(Nsite+1))+k+1]+=-0.5*((pEinA[k,j]*(EinA[k]*EinA[j])**0.5)+(pField[k,j]*(FieldRe[k]*FieldRe[j])**0.5))
                    

    return LindEigen







def BuildPhotonLindblad2(EinA,Field):
    """
    This function produces a lindblad matrix of size (n+1)^2 x (n+1)^2 where n is the number of excited eigenstate of the system. (The +1 is from the groundstate).
    See \cite{Tscherbul2015a}, AnisotropyPaper_CS_resubmit.tex and LightMatter_Lindblad for details.
    
    EinA is a 2D matrix of einstein a coefficient time p
    Field is a 2D matrix of pumping rates if there are no unactive sites
    Field is a 3D matrix of pumping and relaxing rates if there are dark sites
    """
    
    assert len(EinA.shape)==2
    assert len(Field.shape)==2 or len(Field.shape)==3
    
    Nsite=len(EinA)
    if len(Field.shape)==2:
        assert Field.shape==EinA.shape
        FieldEx=Field
        FieldRe=Field
    if len(Field.shape)==3:
        assert len(Field)==2 and Field.shape[1:]==EinA.shape
        FieldEx=Field[0]
        FieldRe=Field[1]
    
    LindEigen=numpy.zeros(shape=((Nsite+1)**2,(Nsite+1)**2))
    
    
        
    for i in range(0,Nsite):
        LindEigen[0,0]+=-FieldEx[i,i] #Bleaching out of \rho_{gg} to \rho_{i,j}
        
        
        for j in range(0,Nsite):
            
            LindEigen[0,((i+1)*(Nsite+1))+j+1]=EinA[i,j]+FieldRe[i,j] #de-excitation and relaxation \rho_{i,j} to \rho_{0,0}
            
            
            
            LindEigen[((i+1)*(Nsite+1)),((j+1)*(Nsite+1))]+=-0.5*(EinA[i,j]+FieldRe[i,j]) # \rho_{g,j} to \rho_{g,i}
            
            LindEigen[((i+1)*(Nsite+1)),((i+1)*(Nsite+1))]+=-0.5*FieldEx[j,j] #Bleaching out of \rho_{g,i} to \rho_{j,i}
            LindEigen[i+1,j+1]+=-0.5*(EinA[i,j]+FieldRe[i,j]) # \rho_{j,g} to \rho_{i,g}
            
            LindEigen[i+1,i+1]+=-0.5*FieldEx[j,j] #Bleaching out of \rho_{i,g} to \rho_{i,j}
            LindEigen[((i+1)*(Nsite+1))+j+1,0]=FieldEx[i,j] #Excitation into \rho_{i,j} from \rho_{gg}
            
            for k in range(0,Nsite):
                LindEigen[((i+1)*(Nsite+1))+j+1,((k+1)*(Nsite+1))+j+1]+=-0.5*(EinA[i,k]+FieldRe[i,k])
                LindEigen[((i+1)*(Nsite+1))+j+1,((i+1)*(Nsite+1))+k+1]+=-0.5*(EinA[k,j]+FieldRe[k,j])
                    

    return LindEigen








def Step_PhotonLindblad(IS,EinA,Field,pEinA,pField,tS):
    
    assert EinA.shape==Field.shape
    assert pEinA.shape==pField.shape
    assert len(EinA.shape)==1
    assert len(pEinA.shape)==2
    assert pEinA.shape==(len(EinA),len(Field))
    
    Nsite=len(EinA)
    Nsys=Nsite+1
    
    assert IS.shape==(Nsys,Nsys)
    assert numpy.all(EinA*tS<0.1)
    assert numpy.all(Field*tS<0.1)
    
    rhoDot=numpy.zeros(shape=(IS.shape),dtype=complex)
    
    for i in range(0,Nsite):
        rhoDot[0,0]+=-pField[i,i]*Field[i]*IS[0,0] #Bleaching out of \rho_{gg} to \rho_{i,j}
        
        
        for j in range(0,Nsite):
            
            rhoDot[0,0]+=((pEinA[i,j]*(EinA[i]*EinA[j])**0.5)+(pField[i,j]*(Field[i]*Field[j])**0.5))*IS[i+1,j+1] #de-excitation and relaxation \rho_{i,j} to \rho_{0,0}
            
            
            
            rhoDot[i+1,0]+=-0.5*((pEinA[i,j]*(EinA[i]*EinA[j])**0.5)+(pField[i,j]*(Field[i]*Field[j])**0.5))*IS[j+1,0] # \rho_{g,j} to \rho_{g,i}
            
            rhoDot[i+1,0]+=-0.5*Field[j]*IS[i+1,0] #Bleaching out of \rho_{gi} to \rho_{j,i}
            rhoDot[0,i+1]+=-0.5*((pEinA[i,j]*(EinA[i]*EinA[j])**0.5)+(pField[i,j]*(Field[i]*Field[j])**0.5))*IS[0,j+1] # \rho_{j,g} to \rho_{i,g}
            
            rhoDot[0,i+1]+=-0.5*Field[j]*IS[0,i+1] #Bleaching out of \rho_{i,g} to \rho_{i,j}
            rhoDot[i+1,j+1]=pField[i,j]*(Field[i]*Field[j])**0.5*IS[0,0] #Excitation into \rho_{i,j} from \rho_{gg}
            
            for k in range(0,Nsite):
                rhoDot[i+1,j+1]+=-0.5*((pEinA[i,k]*(EinA[i]*EinA[k])**0.5)+(pField[i,k]*(Field[i]*Field[k])**0.5))*IS[k+1,j+1]
                rhoDot[i+1,j+1]+=-0.5*((pEinA[k,j]*(EinA[k]*EinA[j])**0.5)+(pField[k,j]*(Field[k]*Field[j])**0.5))*IS[i+1,k+1]
    
    return rhoDot*tS
    
    
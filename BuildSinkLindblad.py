'''
Created on Oct 5, 2016

@author: cs9114
'''

import numpy


def BuildSinkLindblad(Rate,Psink):
    
    Nsite=len(Psink)
    SinkLind=numpy.zeros(shape=((1+Nsite)**2,(1+Nsite)**2))
    
    for i in range(1,Nsite+1):
        SinkLind[0,(i)*(Nsite+1)+i]=2*Psink[i-1,i-1]
#         SinkLind[0,(0)*(Nsite+1)+i]=-Psink[i-1,i-1]
#         SinkLind[0,(i)*(Nsite+1)+0]=-Psink[i-1,i-1]
        
        for j in range(1,Nsite+1,1):
            SinkLind[(i)*(Nsite+1)+j,(i)*(Nsite+1)+j]+=-Psink[i-1,i-1]
            SinkLind[(i)*(Nsite+1)+j,(i)*(Nsite+1)+j]+=-Psink[j-1,j-1]
    
    
    return SinkLind*Rate
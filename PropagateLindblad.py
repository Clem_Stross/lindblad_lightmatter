'''
Created on Sep 19, 2016

@author: cs9114
'''
import numpy
import scipy
from scipy import  linalg
from scipy import sparse
from scipy.sparse import linalg
# from scipy.linalg import expm
# from sympy import Matrix
import math

import pp


from BuildPhotonLindblad import Step_PhotonLindblad
from BuildDephasingLindblad import Step_DephasingLindblad
from BuildHamLindblad import Step_HamLindblad 

from AnalyseDM import AnalyseDM
from AnalyseDM import MesDelocalisation
from AnalyseDM import MesMixing
from AnalyseDM import TraceDistance

def PropagateLindbladEXP(IS,Lind,tM,tS):
    
    assert len(IS.shape)==2
    assert IS.shape[0]==IS.shape[1]
    assert tM>tS
    IS_flat=numpy.ndarray.flatten(IS)
    
    EigenVal,A = numpy.linalg.eig(Lind)
#     A=numpy.transpose(A)
    A1=numpy.linalg.inv(A)
    
    
    NumOfSteps=int(math.ceil(tM/tS))
    
    Output=numpy.zeros(shape=(NumOfSteps,IS.shape[0],IS.shape[1]),dtype=complex)
    
    
    ##### Testing #####
    numpy.testing.assert_allclose(Lind,(A.dot(numpy.diag(EigenVal)).dot(A1)),atol=1e-7)
    numpy.testing.assert_allclose(scipy.linalg.expm(Lind),A.dot(numpy.diag(numpy.exp(EigenVal))).dot(A1),atol=1e-7)
    
    Parallel=True
    if Parallel==False:  #Run code in serial
        for t in range(0,NumOfSteps,1):
            Output[t]=PropagateLindbladEXP_Part((IS_flat,A,EigenVal,A1,t*tS))
#             Output[t]=numpy.reshape(A.dot(numpy.diag(numpy.exp(EigenVal*t*tS))).dot(A1).dot(IS_flat),IS.shape)
#             Output[t]=numpy.reshape(expm(Lind*t*tS).dot(IS_flat),IS.shape)
        return Output
    
    elif Parallel==True: #Run code in parrallel
        
        # tuple of all parallel python servers to connect with
        ppservers = ()
        
        # Creates jobserver with automatically detected number of workers
        job_server = pp.Server(ppservers=ppservers)
         
        Inputs=((IS_flat,A,EigenVal,A1,t*tS) for t in range(0,NumOfSteps,1))
         
         
        print "Starting pp with", job_server.get_ncpus(), "workers"
         
        Jobs=[(job_server.submit(PropagateLindbladEXP_Part, args=(Input,), modules=("numpy",))) for Input in Inputs] #submit jobs
         
        for t in range(0,NumOfSteps,1):
            Output[t]=Jobs[t]() #Get answer for jobs
        
        
        job_server.print_stats()
        
        return Output

def PropagateLindbladEXP_Part(Inputs):
    IS_flat,A,EigenVal,A1,tVal=Inputs
    return numpy.reshape(A.dot(numpy.diag(numpy.exp(EigenVal*tVal))).dot(A1).dot(IS_flat),(len(IS_flat)**0.5,len(IS_flat)**0.5))



###################################################






def SteadyStateMeasurement(Lind,LindDephasing,LindSink,Slider,U):
    
    Output=numpy.zeros(shape=(len(Slider)**2,11))
    if len(Lind)>50:
        SparseMat=True
    else:
        SparseMat=False
    Parallel=True
    
    VarList=[(x,y) for x in Slider for y in Slider]
    
    v0=numpy.zeros(shape=len(Lind))
    v0[0]=1.0
    
    ##### Testing #####
#     print SteadyStateMeasurement_Part((Lind+(LindDephasing)+(LindSink),LindSink))
#     print SteadyStateMeasurement_Sparse_Part((sparse.csr_matrix(Lind+(LindDephasing)+(LindSink)),LindSink))
    
    for i, Var in enumerate(VarList):
        Output[i,0]=Var[0]
        Output[i,1]=Var[1]
    
    if Parallel==False:  #Run code in serial
        if SparseMat==False:
            for i, Var in enumerate(VarList):
                Output[i,2:]=SteadyStateMeasurement_Part((Lind+(Var[1]*LindDephasing)+(Var[0]*LindSink),Var[0]*LindSink,U))
        elif SparseMat==True:
            for i, Var in enumerate(VarList):
#                 print ((Lind+(Var[1]*LindDephasing)+(Var[0]*LindSink)).dot(v0))[0]
                Output[i,2:]=SteadyStateMeasurement_Sparse_Part((sparse.csc_matrix(Lind+(Var[1]*LindDephasing)+(Var[0]*LindSink)),sparse.csc_matrix(Var[0]*LindSink),U))

        
                
        
        return Output
    
    elif Parallel==True: #Run code in parrallel
        
        # tuple of all parallel python servers to connect with
        ppservers = ()
        
        # Creates jobserver with automatically detected number of workers
        job_server = pp.Server(ppservers=ppservers)
         
#         Inputs=((IS_flat,A,EigenVal,A1,t*tS) for t in range(0,NumOfSteps,1))
        
        print "Starting pp with", job_server.get_ncpus(), "workers"
         
        if SparseMat==False:
            Jobs=[(job_server.submit(SteadyStateMeasurement_Part, args=((Lind+(Var[1]*LindDephasing)+(Var[0]*LindSink),Var[0]*LindSink,U),), depfuncs=(null,AnalyseDM,MesDelocalisation,MesMixing,), modules=("numpy","scipy","scipy.linalg","scipy.sparse"))) for Var in VarList] #submit jobs
        elif SparseMat==True:
            Jobs=[(job_server.submit(SteadyStateMeasurement_Sparse_Part, args=((sparse.csc_matrix(Lind+(Var[1]*LindDephasing)+(Var[0]*LindSink)),sparse.csc_matrix(Var[0]*LindSink),U),), depfuncs=(null_Sparse,AnalyseDM,MesDelocalisation,MesMixing,), modules=("numpy","scipy","scipy.sparse.linalg"))) for Var in VarList] #submit jobs
         
        for i, Var in enumerate(VarList):
            Output[i,2:]=Jobs[i]() #Get answer for jobs
            
        job_server.print_stats()
        return Output
        
        




#################Not Sparse
def SteadyStateMeasurement_Part(Inputs):
    Lind,LindSink,U=Inputs
    nSys=len(U)
    
    rhoFlat,rEquib=null(Lind)
    rhoFlat=rhoFlat/numpy.trace(numpy.reshape(rhoFlat,(nSys,nSys)))
#     return numpy.real(LindSink[0].dot(rhoFlat)[0])

    
    LindRelaxVec=Lind[0]
    LindRelaxVec[0]=0
    
    rSink=numpy.abs(LindSink[0].dot(rhoFlat))
    rLoss=numpy.abs(LindRelaxVec.dot(rhoFlat))
    
    return AnalyseDM(numpy.reshape(rhoFlat,(nSys,nSys)),U)+(rSink,rEquib,rSink/rLoss)


def null(A, eps=1e-14):
    u, s, vh = scipy.linalg.svd(A)
    null_mask = (s <= eps)
    null_space = scipy.compress(null_mask, vh, axis=0)

#     u, s, v = numpy.linalg.svd(A)
#     rank = (s > eps*s[0]).sum()
#     print rank
#     return v[rank:].T.copy()

#     scipy.transpose(null_space)

    return null_space[-1],s[-2]-s[-1]



#################Sparse
def SteadyStateMeasurement_Sparse_Part(Inputs):
    
    Lind,LindSink,U=Inputs
    nSys=len(U)
#     nLind=len(LindSink)
    rhoFlat,rEquib=null_Sparse(Lind)

    rhoFlat=rhoFlat/numpy.trace(numpy.reshape(rhoFlat,(nSys,nSys)))
    LindRelax=Lind
    LindRelax[0,0]=0
    
    rSink=numpy.abs(LindSink[0].dot(rhoFlat))
    rLoss=numpy.abs(LindRelax[0].dot(rhoFlat))
    
    return AnalyseDM(numpy.reshape(rhoFlat,(nSys,nSys)),U)+(rSink,rEquib,rSink/rLoss)


def null_Sparse(A, eps=1e-25):
#     v0=numpy.zeros(shape=(6561,))
#     v0[0]=1.0
#     eVal, eVec = scipy.sparse.linalg.eigs(A,k=10,which='LR',ncv=50)
#     v0=numpy.array(shape=(27**2,1))
#     v0[0]=1
    eVal, eVec = scipy.sparse.linalg.eigs(A,k=2,sigma=0+0j,ncv=75)
    
#     return eVec[:,0],numpy.abs(eVal[0])
    
    
    if numpy.abs(eVal[0])<numpy.abs(eVal[1]):
        return eVec[:,0],numpy.abs(eVal[1])-numpy.abs(eVal[0])
    elif numpy.abs(eVal[0])>numpy.abs(eVal[1]):
        return eVec[:,1],numpy.abs(eVal[0])-numpy.abs(eVal[1])





def CompareNullSpace(Linds):
    
    NLinds=len(Linds)
    NSys=len(Linds[0])**0.5
    rhos=[null_Sparse(Lind)[0] for Lind in Linds]
    
    for i in range(0,NLinds,1):
        Rho_i=rhos[i].reshape((NSys,NSys))
        for j in range(i,NLind,1):
            Rho_j=rhos[j].reshape((NSys,NSys))
            
            
            print i,j,TraceDistance(Rho_i,Rho_j)








# def PropagateLindbladDOT(IS,tM,tS,SaveEvery,EinA,Field,pEinA,pField,DephasingRates,Ek,U):
#     
#     assert len(IS.shape)==2
#     assert IS.shape[0]==IS.shape[1]
#     assert tM>tS
#     assert int(SaveEvery)==SaveEvery
#     
#     NumOfSteps=int(math.ceil(tM/tS))
#     NumOfSaves=NumOfSteps/SaveEvery
#     Output=numpy.zeros(shape=(NumOfSaves,IS.shape[0],IS.shape[1]),dtype=complex)
#     
#     Rho_t=IS
#     
#     EigenBasis=False
#     
#     if EigenBasis==True:
#         pass
#         for t1 in range(0,NumOfSaves,1):
#             for t2 in range(0,SaveEvery,1):
#                 Rho_t_Site=U.dot(Rho_t).dot(numpy.transpose(U))
#                 
#                 Rho_Dot=Step_PhotonLindblad(Rho_t,EinA,Field,pEinA,pField,tS)
#                 Rho_Dot+=Step_HamLindblad(Rho_t,Ek,tS)
#                 
#                 Rho_Dot_Site=Step_DephasingLindblad(Rho_t_Site,DephasingRates,tS)
#                 
#                 Rho_t=Rho_t+Rho_Dot+(numpy.transpose(U).dot(Rho_Dot_Site).dot(U))
#                 
#     elif EigenBasis==False:
#         for t1 in range(0,NumOfSaves,1):
#             for t2 in range(0,SaveEvery,1):
#                 
#                 Rho_t_Ein=numpy.transpose(U).dot(Rho_t).dot(U)
#                 
#                 Rho_Dot_Ein=Step_PhotonLindblad(Rho_t_Ein,EinA,Field,pEinA,pField,tS)
#                 Rho_Dot_Ein+=Step_HamLindblad(Rho_t_Ein,Ek,tS)
#                 
#                 Rho_Dot=Step_DephasingLindblad(Rho_t,DephasingRates,tS)
#             
#                 Rho_t=Rho_t+Rho_Dot+(U.dot(Rho_Dot_Ein).dot(numpy.transpose(U)))
#             Output[t1]=Rho_t
#     
#     return Output






if __name__ == '__main__':
    pass
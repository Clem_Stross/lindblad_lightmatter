'''
Created on Oct 10, 2016

@author: cs9114
'''
import os
import numpy
import matplotlib
import matplotlib.pyplot as plt


def PlotSink(Data,UseLog=False,fs=True):
    """
    Col 0 is sink rate
    Col 1 is dephasing rate
    Col -1 is sink efficiency
    """
    nSlider=len(Data)**0.5
    
    if fs==False:
        X=numpy.log10(numpy.reshape(Data[:,0],(nSlider,nSlider)))
        Y=numpy.log10(numpy.reshape(Data[:,1],(nSlider,nSlider)))
    elif fs==True:
        X=numpy.log10(numpy.reshape(Data[:,0]/2.418884326505e-17,(nSlider,nSlider)))
        Y=numpy.log10(numpy.reshape(Data[:,1]/2.418884326505e-17,(nSlider,nSlider)))
        
        
    if UseLog==True:
        Z=numpy.log10(numpy.reshape(Data[:,-1],(nSlider,nSlider)))
    elif UseLog==False:
        Z=numpy.reshape(Data[:,-1],(nSlider,nSlider))
        if numpy.amax(Z)<1:
            pass
        elif numpy.amax(Z)>1 and Z.max()<1.1:
            Z=Z.clip(max=1)
        else:
            print "Efficiency vaules of greater than 1 and is "+str(numpy.amax(Z))
#     Z = numpy.ma.masked_where(-Z <= 0, Z)
    
    
    v1=numpy.linspace(0, 1, 21, endpoint=True)
    v2=numpy.linspace(0, 1, 11, endpoint=True)
#     cs = plt.contourf(X, Y, Z,v1, cmap=matplotlib.cm.PuBu_r)
    cs = plt.contourf(X, Y, Z,v1, cmap=matplotlib.cm.OrRd)
    cbar = plt.colorbar(cs)
    cs = plt.contour(X, Y, Z,v1,linestyles="dotted",colors="k")
    cs = plt.contour(X, Y, Z,v2,linestyles="dotted",colors="k")
    plt.clabel(cs, fmt='%2.1f', colors='k', fontsize=14)
    cbar.ax.set_ylabel(r'Sink Efficiency $\eta$')
    
    

    if fs==False:
        plt.xlabel(r'Measurement Rate Log$_{10}(\Lambda)$')
        plt.ylabel(r'Dephasing Rate Log$_{10}(\Gamma)$')
    elif fs==True:
        plt.xlabel(r'Measurement Rate Log$_{10}(\Lambda)$ $\quad s^{-1}$')
        plt.ylabel(r'Dephasing Rate Log$_{10}(\Gamma)$ $\quad s^{-1}$')
    
    return plt



def PlotData(Data,UseLog=False,fs=True):
    """
    Col 0 is sink rate
    Col 1 is dephasing rate
    Col -1 is sink efficiency
    """
    nSlider=len(Data)**0.5
    
    if fs==False:
        X=numpy.log10(numpy.reshape(Data[:,0],(nSlider,nSlider)))
        Y=numpy.log10(numpy.reshape(Data[:,1],(nSlider,nSlider)))
    elif fs==True:
        X=numpy.log10(numpy.reshape(Data[:,0]/2.418884326505e-17,(nSlider,nSlider)))
        Y=numpy.log10(numpy.reshape(Data[:,1]/2.418884326505e-17,(nSlider,nSlider)))
        
    
    Labels=(r"1-$\langle \Phi^{(0)} | \rho | \Phi^{(0)} \rangle$",r"Mixing tr$\rho^{2}_{e}$", r"Localisation $P^{-1}_{Sites}$",
             r"Localisation $P^{-1}_{Eigen}$", r"Dephasing $D_{Sites}$", r"Dephasing $D_{Eigen}$",
             r"log$_{10}(\langle \Phi^{(0)} | \mathcal{L}_{Sink} \cdot \rho | \Phi^{(0)} \rangle)$ $\quad s^{-1}$", r"tr$P_{Sink}\rho_{e}$",r"log$_{10}$ Rate to Equib $\quad s^{-1}$",)
    ColorList=(matplotlib.cm.Blues,matplotlib.cm.BuGn,matplotlib.cm.BuPu,matplotlib.cm.BuPu,matplotlib.cm.YlOrRd,matplotlib.cm.YlOrRd,matplotlib.cm.YlOrBr,matplotlib.cm.Blues,matplotlib.cm.PuBuGn)
    LimitList=[[None,None],[None,None],[None,None],[None,None],[None,None],[None,None],[None,None],[None,None],[None,None]]
    
    fig = plt.figure(figsize=(12, 10), dpi=100)
    for PlotNum in range(0,len(Labels),1):
        plt.subplot(3,3,PlotNum+1)
        
        if PlotNum==6:
            Z=numpy.log10(numpy.reshape(Data[:,PlotNum+2]/2.418884326505e-17,(nSlider,nSlider)))
        elif PlotNum==8:
            Z=numpy.log10(numpy.reshape(Data[:,9]/2.418884326505e-17,(nSlider,nSlider)))
        elif PlotNum==7:
            Z=numpy.reshape((Data[:,8]/(2*Data[:,0]*Data[:,2])),(nSlider,nSlider))
        elif PlotNum>7:
            Z=numpy.reshape(Data[:,PlotNum+1],(nSlider,nSlider))
        else:
            Z=numpy.reshape(Data[:,PlotNum+2],(nSlider,nSlider))
        
    
#         v1=numpy.linspace(0, 1, 21, endpoint=True)
#         v2=numpy.linspace(0, 1, 11, endpoint=True)
#         cs = plt.contourf(X, Y, Z,v1, cmap=matplotlib.cm.PuBu_r)
        cs = plt.contourf(X, Y, Z, cmap=ColorList[PlotNum],vmin=LimitList[PlotNum][0], vmax=LimitList[PlotNum][1])
        cbar = plt.colorbar(cs)
        
        
        cs = plt.contour(X, Y, Z,linestyles="dotted",colors="k",vmin=LimitList[PlotNum][0], vmax=LimitList[PlotNum][1])
#         plt.clabel(cs, fmt='%2.1f', colors='k', fontsize=14)
        
        
        
    
        if PlotNum==7:
            plt.xlabel(r'Measurement Rate Log$_{10}(\Lambda)$ $\quad s^{-1}$')
        if PlotNum==3:
            plt.ylabel(r'Dephasing Rate Log$_{10}(\Gamma)$ $\quad s^{-1}$')
        
        plt.title(Labels[PlotNum],fontsize=12)
#         cbar.ax.set_ylabel(Labels[PlotNum])
    return plt


def PlotRaft(Pos):
    
    Site=numpy.zeros(shape=(0,3))
    SinkSite=numpy.zeros(shape=(0,3))
    InactiveSite=numpy.zeros(shape=(0,3))
    InactiveSinkSite=numpy.zeros(shape=(0,3))
    
    
    for BChl in Pos:
        if BChl[-1]==0:
            Site=numpy.vstack((Site,BChl[0:3]))
        elif BChl[-1]==1:
            SinkSite=numpy.vstack((SinkSite,BChl[0:3]))
        elif BChl[-1]==10:
            InactiveSite=numpy.vstack((InactiveSite,BChl[0:3]))
        elif BChl[-1]==11:
            InactiveSinkSite=numpy.vstack((InactiveSinkSite,BChl[0:3]))
        else:
            print "in PlotRaft in PlotSink ID not found"
    
#     Site=numpy.array([[1,1,1],[2,2,2]])
    
    
    sc=plt.scatter(Site[:,1], Site[:,2], c=Site[:,0],s=500,cmap='gray',marker="o",label="Site",vmin=numpy.amin(Pos[:,0]),vmax=numpy.amax(Pos[:,0]))
    plt.scatter(SinkSite[:,1], SinkSite[:,2], c=SinkSite[:,0],s=500,cmap='gray',marker="D",label="Sink",vmin=numpy.amin(Pos[:,0]),vmax=numpy.amax(Pos[:,0]))
    if len(InactiveSite)>0:
        plt.scatter(InactiveSite[:,1], InactiveSite[:,2], c=InactiveSite[:,0],s=500,cmap='gray',marker="s",label="Inactive Site",vmin=numpy.amin(Pos[:,0]),vmax=numpy.amax(Pos[:,0]))
    if len(InactiveSinkSite)>0:
        plt.scatter(InactiveSinkSite[:,1], InactiveSinkSite[:,2], c=InactiveSinkSite[:,0],s=500,cmap='gray',marker="p",label="Inactive Sink",vmin=numpy.amin(Pos[:,0]),vmax=numpy.amax(Pos[:,0]))
    plt.legend()
    cbar=plt.colorbar(sc)
    cbar.set_label("x")
    plt.xlabel("y")
    plt.ylabel("z")
    
    plt.axes().set_aspect('equal', 'datalim')
    plt.grid()
    
    return plt
    
    


if __name__=="__main__":
    
    for file in os.listdir("./"):
        if file.startswith("SinkEfficiency_") and file.endswith(".txt"):
            SinkFig=PlotSink(numpy.loadtxt("./"+file))
            SinkFig.savefig(file[:-4]+'.pdf')
            SinkFig.close()
            
            DataFig=PlotData(numpy.loadtxt("./"+file))
            DataFig.savefig('NullSpaceInfo_'+file[15:-4]+'.pdf')
            DataFig.close()
            
        if file.startswith("RSites_") and file.endswith(".txt"):
            RaftFig=PlotRaft(numpy.loadtxt("./"+file))
            RaftFig.savefig('RaftFig_'+file[7:-4]+'.pdf')
            RaftFig.close()
    
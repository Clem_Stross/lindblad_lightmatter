#!/opt/local/bin/python2.7
import sys
sys.path.insert(0, '/Users/cs9114/Documents/PhD_NonDropbox/LHII-Light_Interaction/JaynesCummings/Python/JaynesCummings_Code')
import os
import numpy
from scipy import sparse
from scipy.stats import threshold

from Read_InputFile import ReadFromFile
from LHII_Ham import LHII_Ham
from PhotonPrameters import PhotonPrameters
from BuildPhotonLindblad import BuildPhotonLindblad
from BuildPhotonLindblad import BuildPhotonLindblad2
from BuildDephasingLindblad import BuildDephasingLindblad
from BuildLindbladUnitary import BuildLindUnitary
from BuildHamLindblad import BuildHamLindblad
from BuildSinkLindblad import BuildSinkLindblad

from Construct_Lind import ConstructLind_HamDependent

from PropagateLindblad import PropagateLindbladEXP
# from PropagateLindblad import PropagateLindbladDOT
from PropagateLindblad import SteadyStateMeasurement
from PlotOutput import PlotOutputData
from PlotOutput import Flatten3DImagArrayToSave

from PlotSink import PlotSink
from PlotSink import PlotData
from PlotSink import PlotRaft

def RunLightMatter(InFile):
    
    
    Ham=ReadFromFile("Ham",InFile,"List",0)
    Mu=ReadFromFile("Mu",InFile,"List",0)
    
    HamType=ReadFromFile("HamType",InFile,"String",0)
    
    Temp=ReadFromFile("Temp",InFile,"Float",0)
    KbT=ReadFromFile("KbT",InFile,"Float",0)
    if KbT*Temp==KbT+Temp:
        print "Input must include 'Temp' or 'KbT'"
        sys.exit()
    elif Temp!=0 and KbT==0:
        KbT=6000*3.166811429*(10**-6)
    print KbT
    
    k0=ReadFromFile("k0",InFile,"List",numpy.array([0]))
    
    
    SinkSites=ReadFromFile("SinkSites",InFile,"List",numpy.array([]))
    InactiveSites=ReadFromFile("InactiveSites",InFile,"List",numpy.array([]))

        
        
    #===========================================================================
    # Reading in system information from inputfile
    #===========================================================================
    if type(Ham)!=int and type(Mu)!=int and HamType==0:
        assert int(len(Ham)**0.5)==len(Ham)**0.5
        assert int(len(Mu)/3)==len(Mu)/3
        assert int(len(Mu)/3)==int(len(Ham)**0.5)
        Mu=Mu.reshape(len(Mu)/3,3)
        Ham=Ham.reshape((len(Ham)**0.5,len(Ham)**0.5))
        Rsite=numpy.zeros(shape=Mu.shape)
    
    
    #===========================================================================
    # Creating system information from pdb file or from ideal structure.
    #===========================================================================
    elif type(Ham)==int and type(Mu)==int and type(HamType)==str:
        Ham,Mu,Rsite=LHII_Ham(InFile)
        LHII_Num=ReadFromFile("LHII_Num",InFile,"int",1)
        LH1at=ReadFromFile("LH1at",InFile,"List",numpy.array([]))
        LH1at=[int(LH1) for LH1 in LH1at]
        
        
        N_BChl=len(Ham)
        if LHII_Num>LH1at:
            BChlPerLHII=N_BChl-(36*len(LH1at))/(LHII_Num-len(LH1at))
        else:
            BChlPerLHII=0
        
        RaftBreaks=[36 if x in LH1at else BChlPerLHII for x in range(0,LHII_Num,1)]
        RaftBreaks=[0]+RaftBreaks
        RaftInfo=[range(sum(RaftBreaks[:x+1]),sum(RaftBreaks[:x+2]),1) for x in range(0,LHII_Num,1)]
        
        if len(LH1at)>0:
            assert LHII_Num>numpy.amax(LH1at)
            for LH1 in LH1at:
                SinkSites=numpy.hstack((SinkSites,numpy.array(RaftInfo[LH1][0:4])))
        
        InactiveLHII=ReadFromFile("InactiveSites",InFile,"List",numpy.array([]))
        if len(InactiveLHII)>0:
            for LH1 in InactiveLHII:
                InactiveSites=numpy.hstack((InactiveSites,numpy.array(RaftInfo[LH1])))
        
    
    Vmax=numpy.amax(numpy.abs(Ham-numpy.diag(numpy.diag(Ham))))
    VMin=numpy.amin(numpy.abs(Ham-numpy.diag(numpy.diag(Ham))))
    Cutoff=1e-2
    if Vmax*Cutoff>VMin:
        print "Ham Cutoff used"
        Ham=Ham*threshold(threshold(numpy.abs(Ham),threshmin=Vmax*Cutoff,newval=0),threshmax=Vmax*Cutoff,newval=1)
        
    HamSites=numpy.zeros(shape=(Ham.shape[0]+1,Ham.shape[1]+1))
    HamSites[1:,1:]=Ham
    
    Ek,Uk=numpy.linalg.eigh(Ham)
    Uk=numpy.transpose(Uk)
    
    
    HamEigen=numpy.zeros(shape=(len(Ek)+1,len(Ek)+1))
    HamEigen[1:,1:]=numpy.diag(Ek)
    
    U=numpy.eye(len(Ek)+1)
    U[1:,1:]=Uk
    
    
    EinA,Field,pEinA,pField=PhotonPrameters(Ek,Uk,Mu,KbT,k0,InactiveSites)
    DephasingRate=ReadFromFile("DephasingRate",InFile,"Float")*numpy.ones(shape=EinA.shape)
    
    SinkRate=ReadFromFile("SinkRate",InFile,"Float",0)
    Psink=numpy.zeros(shape=(len(Ek),len(Ek)))
    if SinkRate>=0:
        
        
        
        
        
        for i in SinkSites:
            Psink[i,i]=1.0
            
        SliderValues=ReadFromFile("SliderValues",InFile,"List",numpy.array([]))
        if len(SliderValues)>0:
                LindPropByExp=False
                SteadStateSink=True
        else:
            LindPropByExp=True
            SteadStateSink=False
    else:
        LindPropByExp=True
        SteadStateSink=False
    
    LossRate=ReadFromFile("LossRate",InFile,"Float",0)
    
    
#     
#     EinA=numpy.array([10.,10.,10])
#     Field=numpy.array([0.1,0.1,0.1])
#     p=numpy.array([[1.,1.,1.],[1.,1.,1],[1.,1.,1.]])
#     DephasingRate=numpy.array([10.0,10.0,10.0])
    
    Rsite_Data=numpy.zeros(shape=(len(Rsite),4))
    Rsite_Data[:,0:3]=Rsite
    for Site in SinkSites:
        Rsite_Data[Site,3]+=1
    for Site in InactiveSites:
        Rsite_Data[Site,3]+=10
    
    EigenBasis=False
    
    nSite=len(EinA)
    
    
    
    
    
    LindDephasing=BuildDephasingLindblad(DephasingRate)
    
    LindSink=BuildSinkLindblad(SinkRate,Psink)
    LindLoss=BuildSinkLindblad(LossRate,numpy.identity(nSite))
    
    
    ####### Different ways to do eigenstates
    FastEigenstate=True
    if FastEigenstate:
        EinA_Mat_Eigen=numpy.outer(EinA**0.5,EinA**0.5)*pEinA
        FieldEx_Mat_Eigen=numpy.outer(Field[0]**0.5,Field[0]**0.5)*pField
        FieldRe_Mat_Eigen=numpy.outer(Field[1]**0.5,Field[1]**0.5)*pField
        
        EinA_Mat_Site=numpy.transpose(Uk).dot(EinA_Mat_Eigen).dot(Uk)
        FieldEx_Mat_Site=numpy.transpose(Uk).dot(FieldEx_Mat_Eigen).dot(Uk)
        FieldRe_Mat_Site=numpy.transpose(Uk).dot(FieldRe_Mat_Eigen).dot(Uk)
        
        LindPhoton=BuildPhotonLindblad2(EinA_Mat_Site,numpy.array([FieldEx_Mat_Site,FieldRe_Mat_Site]))
        LindHam=BuildHamLindblad(HamSites)
        LindPhoton=LindPhoton*threshold(threshold(numpy.abs(LindPhoton),threshmin=1e-18,newval=0),threshmax=1e-18,newval=1)
        LindHam=LindHam*threshold(threshold(numpy.abs(LindHam),threshmin=1e-10,newval=0),threshmax=1e-10,newval=1)
        
        
#     else: ####### Different ways to do eigenstates
        LindPhoton1=BuildPhotonLindblad(EinA,Field,pEinA,pField)
        LindHam1=BuildHamLindblad(Ek)
        
        LU=BuildLindUnitary(numpy.transpose(U))
        LindPhoton1=(numpy.transpose(LU).dot(LindPhoton1).dot(LU))
        LindPhoton1=LindPhoton1*threshold(threshold(numpy.abs(LindPhoton1),threshmin=1e-18,newval=0),threshmax=1e-18,newval=1)
        
        LindHam1=numpy.transpose(LU).dot(LindHam1).dot(LU)
        LindHam1=LindHam1*threshold(threshold(numpy.abs(LindHam1),threshmin=1e-10,newval=0),threshmax=1e-10,newval=1)
        
        
        print numpy.linalg.norm(LindPhoton-LindPhoton1),numpy.linalg.norm(LindPhoton),numpy.linalg.norm(LindPhoton1)
        print numpy.linalg.norm(LindHam-LindHam1),numpy.linalg.norm(LindHam),numpy.linalg.norm(LindHam1)
        
        LindPhoton=LindPhoton1
        LindHam=LindHam1
        
#     else:
        LindPhoton2,LindHam2,U=ConstructLind_HamDependent(Ham,Mu,InactiveSites)
        
        print numpy.linalg.norm(LindPhoton-LindPhoton2),numpy.linalg.norm(LindPhoton),numpy.linalg.norm(LindPhoton2)
        print numpy.linalg.norm(LindHam-LindPhoton2),numpy.linalg.norm(LindHam),numpy.linalg.norm(LindPhoton2)
        
        sys.exit()
        
        
    print "Lindblad matrix sparsity="+str(float(numpy.count_nonzero(LindPhoton+LindHam+LindDephasing+LindSink+LindLoss))/(len(LindPhoton)**2))+"\n"
    
    if LindPropByExp==True:
        
        rho0=numpy.zeros(shape=(nSite+1,nSite+1))
        rho0[0,0]=1
        
        tMax=ReadFromFile("tMax",InFile,"Float")
        tStep=ReadFromFile("tStep",InFile,"Float")
        
        LindTot=LindPhoton+LindHam+LindDephasing+LindLoss+LindSink
        SiteDM=PropagateLindbladEXP(rho0,LindTot,tMax,tStep)
        
        return SiteDM,Uk,Rsite_Data


    elif SteadStateSink==True:
        
        Output=SteadyStateMeasurement(LindPhoton+LindHam+LindLoss,LindDephasing,LindSink,SliderValues,U)
        Output[:,0]*=SinkRate
        Output[:,1]*=DephasingRate[0]
        
        return Output,Rsite_Data
    
    
    
    

    
if __name__=="__main__":
#     main("")
    
    if len(sys.argv)==1:
        InputFile="./Testing_MD_Input.txt"
#         InputFile="./Testing_Sites_Input.txt""
    else:
        InputFile=sys.argv[1]
        
    OutputSuffix=ReadFromFile("OutputSuffix",InputFile,"String")
    if not os.path.exists('./'+OutputSuffix):
        os.makedirs('./'+OutputSuffix)
        
    
    cwd = os.getcwd()
    if "Documents" in cwd:
        MakeFig=True
    else:
        MakeFig=False
    
    
    SliderValues=ReadFromFile("SliderValues",InputFile,"List",numpy.array([]))
    if len(SliderValues)==0:
        PopSim=True
    else:
        PopSim=False
        
    if PopSim==True:
        SiteDM,Uk,Rsite=RunLightMatter(InputFile)
    
        tMax=ReadFromFile("tMax",InputFile,"Float")
        tStep=ReadFromFile("tStep",InputFile,"Float")
        LHII_Num=ReadFromFile("LHII_Num",InputFile,"Float",0)
    
        numpy.savetxt('./'+OutputSuffix+'/TimeValues_'+OutputSuffix+'.txt',numpy.array([tMax,tStep,LHII_Num,0]),fmt='%.6g')
        numpy.savetxt('./'+OutputSuffix+'/SiteDM_'+OutputSuffix+'.txt',Flatten3DImagArrayToSave(SiteDM),fmt='%.6g')
        numpy.savetxt('./'+OutputSuffix+'/RSites_'+OutputSuffix+'.txt',Rsite,fmt='%.6g')
        
        if MakeFig:
            PlotOutputData(SiteDM,tMax,tStep,U=Uk,OutputSuffix=OutputSuffix)
    else:
        
        Output,Rsite=RunLightMatter(InputFile)
        
        numpy.savetxt('./'+OutputSuffix+'/SinkEfficiency_'+OutputSuffix+'.txt',Output,fmt='%.6g')
        
        if MakeFig:
            SinkFig=PlotSink(Output)
            SinkFig.savefig(OutputSuffix+'/SinkEfficiency_'+OutputSuffix+'.pdf')
            SinkFig.close()
            DataFig=PlotData(Output)
            DataFig.savefig(OutputSuffix+'/NullSpaceInfo_'+OutputSuffix+'.pdf')
            DataFig.close()
            
        if numpy.linalg.norm(Rsite.flatten())>0:
            numpy.savetxt('./'+OutputSuffix+'/RSites_'+OutputSuffix+'.txt',Rsite,fmt='%.6g')
                
            if MakeFig:
                RaftFig=PlotRaft(Rsite)
                RaftFig.savefig('./'+OutputSuffix+'/RaftFig_'+OutputSuffix+'.pdf')
                DataFig.close()
    print "Job Done"
    
import numpy

def AnalyseDM(DM,U):
    
    
    DMe=DM[1:,1:]
    DMe=DMe/numpy.abs(numpy.trace(DMe))
    
    DM_Eigen=U.dot(DM).dot(numpy.transpose(numpy.conj(U)))
    DMe_Eigen=DM_Eigen[1:,1:]/numpy.trace(DM_Eigen[1:,1:])
    
    Deloc_Site=MesDelocalisation(DMe)
    Deloc_Ein=MesDelocalisation(DMe_Eigen)
    Mix=MesMixing(DMe)
    
    return 1-numpy.abs(DM[0,0]),Mix,Deloc_Site,Deloc_Ein,(Deloc_Site*Mix)**-1,(Deloc_Ein*Mix)**-1
    

def MesDelocalisation(rho):
    return numpy.abs(numpy.trace(rho)**2/numpy.trace(rho*numpy.conj(rho)))

def MesMixing(rho):
    return numpy.abs(numpy.trace(rho.dot(rho))/numpy.trace(rho)**2)

def MesDephasing(rho):
    (MesDelocalisation(rho)*MesMixing(rho))**-1

def TraceDistance(rho1,rho2):
    
    return numpy.sum(numpy.abs(numpy.linalg.eigvalsh((rho1/numpy.tr(rho1))-(rho2/numpy.tr(rho2)))))

'''
Created on Sep 21, 2016

@author: cs9114
'''

import numpy


def TransitionDipoleMoments(U,MuSite):
    
    nSites=len(U)
    assert U.shape==(nSites,nSites)
    assert MuSite.shape==(nSites,3)
    
    MuEigen=numpy.zeros(shape=MuSite.shape)
    
    for i in range(0,nSites,1):
        for j in range(0,nSites,1):
            MuEigen[i]+=U[i,j]*MuSite[j]
            
    MuMagCutoff=numpy.amax(MuEigen)*1e-10
    for i in range(0,nSites,1):
        if numpy.linalg.norm(MuEigen[i])<MuMagCutoff:
            MuEigen[i]=0
            
    return MuEigen
            
def Build_EinA(Ek,MuEigen):
    
    nSites=len(Ek)
    assert MuEigen.shape==(nSites,3)
    
    MuEigenMag=numpy.array([numpy.linalg.norm(MuEigen[i]) for i in range(0,nSites,1)])
    return (4*numpy.pi*(Ek**3)*(MuEigenMag**2))/(3*(137**3))


def Build_Field(Ek,MuEigen,KbT,RadiationDensity=5e-6):
    """
    Takes EigenEnergies Ek, Eigenstate transition dipole moments and returns the excitation from the feild
    kBT is the temprature times the boltxman's constant.
    RadiationDensity is the scaling to modify the answer to account for the distance between the sun and earth. In reality is very small number.
    """
    
    nSites=len(Ek)
    assert MuEigen.shape==(nSites,3)
    assert KbT>0
    assert type(RadiationDensity)==type(1) or type(RadiationDensity)==type(1.0)
    
    MuEigenMag=numpy.array([numpy.linalg.norm(MuEigen[i]) for i in range(0,nSites,1)])
    
    return numpy.abs(RadiationDensity*((1-numpy.exp(Ek/KbT))**-1)*((4*(Ek**3)*(MuEigenMag**2))/(3*(137**3))))


def Build_pMatrix_Isotropic(MuEigen):
    
    nSites=len(MuEigen)
    assert MuEigen.shape==(nSites,3)
    
    MuEigenMag=numpy.array([numpy.linalg.norm(Mu) for Mu in MuEigen])
    
    pMat=numpy.zeros(shape=(nSites,nSites))
    for i in range(0,nSites,1):
        pMat[i,i]=1.0
        if MuEigenMag[i]>0:
            for j in range(i+1,nSites,1):
                if MuEigenMag[j]>0:
                    pMat[i,j]=MuEigen[i].dot(MuEigen[j])/(MuEigenMag[i]*MuEigenMag[j])
                    pMat[j,i]=pMat[i,j]
            
    return pMat


def Build_pMatrix_k0(MuEigen,k0):
    
    nSites=len(MuEigen)
    assert MuEigen.shape==(nSites,3)
    
    if numpy.linalg.norm(k0)==0:
        return Build_pMatrix_Isotropic(MuEigen)
    
    else:
        assert k0.shape==(3,)
    
    MuEigenMag=numpy.array([numpy.linalg.norm(Mu) for Mu in MuEigen])
    
    pMat=numpy.zeros(shape=(nSites,nSites))
    kMag=numpy.linalg.norm(k0)
    
    
    for i in range(0,nSites,1):
        if MuEigenMag[i]>0:
            pMat[i,i]=numpy.cross(MuEigen[i],k0).dot(numpy.cross(MuEigen[i],k0))/(MuEigenMag[i]*MuEigenMag[i]*kMag*kMag)
            for j in range(i+1,nSites,1):
                if MuEigenMag[j]>0:
                    pMat[i,j]=numpy.cross(MuEigen[i],k0).dot(numpy.cross(MuEigen[j],k0))/(MuEigenMag[i]*MuEigenMag[j]*kMag*kMag)
                    pMat[j,i]=pMat[i,j]
    return pMat


def PhotonPrameters(Ek,U,MuSite,KbT,k0,InactiveSites=numpy.array([])):
    
    
    nSites=len(Ek)
    assert U.shape==(nSites,nSites)
    assert MuSite.shape==(nSites,3)
    assert len(k0)==3 or len(k0)==1 or len(k0)==0
#     if len(k0.shape)==1:
#         assert k0[0]==0
    assert KbT>0
    
    
    MuEigen=TransitionDipoleMoments(U,MuSite)
    
    
    if len(InactiveSites)>0:
        MuSite_Ex=MuSite
        for Site in InactiveSites:
            MuSite_Ex[Site]=0
        MuEigen_Ex=TransitionDipoleMoments(U,MuSite_Ex)
        return Build_EinA(Ek,MuEigen),numpy.array([Build_Field(Ek,MuEigen_Ex,KbT),Build_Field(Ek,MuEigen,KbT)]),Build_pMatrix_Isotropic(MuEigen),Build_pMatrix_k0(MuEigen,k0)
    else:
        return Build_EinA(Ek,MuEigen),Build_Field(Ek,MuEigen,KbT),Build_pMatrix_Isotropic(MuEigen),Build_pMatrix_k0(MuEigen,k0)

    
    

if __name__ == '__main__':
    pass
import numpy
from scipy.stats import threshold

from PhotonPrameters import PhotonPrameters

from BuildPhotonLindblad import BuildPhotonLindblad
from BuildPhotonLindblad import BuildPhotonLindblad2
from BuildHamLindblad import BuildHamLindblad

def ConstructLind_HamDependent(Ham_Site,Mu_Site,InactiveSites=numpy.array([]),Cutoff=1e-2):
    
    assert len(Ham_Site.shape)==2
    assert len(Ham_Site)==len(Mu_Site) or len(Ham_Site)==len(Mu_Site)+1
    
    if Cutoff>0:
        Vmax=numpy.amax(numpy.abs(Ham_Site-numpy.diag(numpy.diag(Ham_Site))))
        VMin=numpy.amin(numpy.abs(Ham_Site-numpy.diag(numpy.diag(Ham_Site))))
        if Vmax*Cutoff>VMin:
            print "Ham Cutoff used"
            Ham_Site=Ham_Site*threshold(threshold(numpy.abs(Ham_Site),threshmin=Vmax*Cutoff,newval=0),threshmax=Vmax*Cutoff,newval=1)
        
        
    if len(Ham_Site)==len(Mu_Site):
        Ham_g=numpy.zeros(shape=(len(Ham_Site)+1,len(Ham_Site)+1))
        Ham_g[1:,1:]=Ham_Site
        Ham_e=Ham_Site
    elif len(Ham_Site)==len(Mu_Site)+1:
        Ham_g=Ham_Site
        Ham_e=Ham_Site[1:,1:]
        
    
    Ek,Uk=numpy.linalg.eigh(Ham_e)
    Uk=numpy.transpose(Uk)
    U=numpy.eye(len(Ek)+1)
    U[1:,1:]=Uk
    
#     HamEigen=numpy.zeros(shape=(len(Ek)+1,len(Ek)+1))
#     HamEigen[1:,1:]=numpy.diag(Ek)
    
    KbT=0.019000868574
    k0=numpy.array([1,0,0])
    print Ek.shape,Uk.shape,Mu_Site.shape
    EinA,Field,pEinA,pField=PhotonPrameters(Ek,Uk,Mu_Site,KbT,k0,InactiveSites)
    
    
    EinA_Mat_Eigen=numpy.outer(EinA**0.5,EinA**0.5)*pEinA
    FieldEx_Mat_Eigen=numpy.outer(Field[0]**0.5,Field[0]**0.5)*pField
    FieldRe_Mat_Eigen=numpy.outer(Field[1]**0.5,Field[1]**0.5)*pField
    
    EinA_Mat_Site=numpy.transpose(Uk).dot(EinA_Mat_Eigen).dot(Uk)
    FieldEx_Mat_Site=numpy.transpose(Uk).dot(FieldEx_Mat_Eigen).dot(Uk)
    FieldRe_Mat_Site=numpy.transpose(Uk).dot(FieldRe_Mat_Eigen).dot(Uk)
    
    LindPhoton=BuildPhotonLindblad2(EinA_Mat_Site,numpy.array([FieldEx_Mat_Site,FieldRe_Mat_Site]))
    
    LindHam=BuildHamLindblad(Ham_g)
    
    
#     LindPhoton=LindPhoton*threshold(threshold(numpy.abs(LindPhoton),threshmin=1e-18,newval=0),threshmax=1e-18,newval=1)
#     LindHam=LindHam*threshold(threshold(numpy.abs(LindHam),threshmin=1e-10,newval=0),threshmax=1e-10,newval=1)
    
    return LindPhoton,LindHam,U



    
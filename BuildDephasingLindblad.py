'''
Created on Sep 19, 2016

@author: cs9114
'''
import numpy

def BuildDephasingLindblad(DephasingRates):
    """
    Build a lindblad for dephasing in the site basis. State ordering is ground-> then excited
    """
    
    assert len(DephasingRates.shape)==1
    
    Nsite=len(DephasingRates)
    Nsys=Nsite+1
    Lind=numpy.zeros(shape=(Nsys**2,Nsys**2))
    
    for i in range(1,Nsys,1):
        Lind[(i*Nsys)+0,(i*Nsys)+0]=-0.5*(DephasingRates[i-1])
        Lind[(0*Nsys)+i,(0*Nsys)+i]=Lind[(i*Nsys)+0,(i*Nsys)+0]
        for j in range(i+1,Nsys,1):
            Lind[(i*Nsys)+j,(i*Nsys)+j]=-0.5*(DephasingRates[i-1]+DephasingRates[j-1])
            Lind[(j*Nsys)+i,(j*Nsys)+i]=Lind[(i*Nsys)+j,(i*Nsys)+j]


    return Lind

def Step_DephasingLindblad(IS,DephasingRates,tS):
    """
    Build a lindblad for dephasing in the site basis. State ordering is ground-> then excited
    """
    
    assert len(DephasingRates.shape)==1
    assert numpy.all(DephasingRates*tS<0.1)
    
    Nsite=len(DephasingRates)
    Nsys=Nsite+1
    
    assert IS.shape==(Nsys,Nsys)
    rhoDot=numpy.zeros(shape=(IS.shape),dtype=complex)
    
    for i in range(1,Nsys,1):
        rhoDot[0,i]=-0.5*(DephasingRates[i-1])*IS[0,i]
        rhoDot[i,0]=-0.5*(DephasingRates[i-1])*IS[i,0]
        for j in range(i+1,Nsys,1):
            rhoDot[i,j]=-0.5*(DephasingRates[i-1]+DephasingRates[j-1])*IS[i,j]
            rhoDot[j,i]=-0.5*(DephasingRates[i-1]+DephasingRates[j-1])*IS[j,i]


    return rhoDot*tS

if __name__ == '__main__':
    pass